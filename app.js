const app = require("express")();
const port = process.env.port || 8000;

app.get("/",(req,res)=>{
   res.send("Well done!! you have just run your first dockerised application")
});

app.listen(port, () => {
    console.log("Express server has started listening on port 8000...")
});
