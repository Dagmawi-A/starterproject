#dockerfile for development enviroment
#pull base image
FROM node:13-alpine


#create working dir
RUN  cd /usr && mkdir src &&  cd /usr/src/ && mkdir app

#set application working dir
WORKDIR /usr/src/app

#copy package.json and package-lock.json
COPY package*.json ./

#install depenacies
RUN npm install

#copy project files
COPY . .

#expose port
EXPOSE 8000

#runs dev server
CMD npm run start
